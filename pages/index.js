import Router from "next/router";
import { useState, useEffect } from "react";
import { todoService } from "todo-shared";

const Index = props => {
  const [todos, setTodos] = useState([]);
  useEffect(() => {
    (async () => {
      if (!sessionStorage.getItem("userKey")) {
        Router.push("/login");
      } else {
        const token = sessionStorage.getItem("userKey");
        const todos = await todoService(token).getAllTodos();
        setTodos(todos);
      }
    })();
  }, []); // https://github.com/facebook/react/issues/14100
  return todos.map(todo => {
    return <div key={todo.id}>{todo.title}</div>;
  });
};

// Index.getInitialProps = async function() {};

export default Index;
