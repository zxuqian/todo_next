import { useState } from "react";
import { connect } from "react-redux";
import Router from "next/router";

import sessionSlice from "../slices/session";

const Login = props => {
  const { loginUser } = props;
  const [identifier, setIdentifier] = useState("");
  const [password, setPassword] = useState("");
  const onLogin = async event => {
    event.preventDefault();
    loginUser({ identifier, password }, user => {
      sessionStorage.setItem("userKey", user.jwt);
      Router.push("/");
    });
    // if (user.jwt) {
    //   loginUser(user.user);
    //   sessionStorage.setItem("userKey", user.jwt);
    //   Router.push("/");
    // }
  };
  return (
    <div>
      <form>
        <label>用户名：</label>
        <input
          type="text"
          name="identifier"
          value={identifier}
          onChange={event => setIdentifier(event.target.value)}
        />
        <label>密码：</label>
        <input
          type="password"
          name="password"
          value={password}
          onChange={event => setPassword(event.target.value)}
        />
        <button onClick={onLogin} type="submit">
          登录
        </button>
      </form>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    session: state.session
  };
};
const mapDispatchToProps = { loginUser: sessionSlice.actions.loginUser };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
