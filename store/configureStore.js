import { configureStore, getDefaultMiddleware } from "redux-starter-kit";
import sessionSlice from "../slices/session";

const reducer = {
  session: sessionSlice.reducer
};

const middleware = [...getDefaultMiddleware()];

const store = configureStore({
  reducer,
  middleware,
  devTools: true
});

export default store;
